export default interface Category {
  id: string | number;
  name: string;
  parent?: string | number;
  parentName?: string;
};
