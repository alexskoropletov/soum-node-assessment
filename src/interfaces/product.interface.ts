import { Image } from '.';

export default interface Product {
  id: string | number;
  name: string;
  price: number;
  category?: string;
  state?: string;
  images?: Image[];
};
