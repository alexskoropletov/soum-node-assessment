export default interface ProductImage {
  productId: string;
  imageId: string;
  main: boolean;
};
