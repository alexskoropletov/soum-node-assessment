export default interface ProductStateHistory {
  productId: string;
  stateId: string;
  datetime: Date | string;
};
