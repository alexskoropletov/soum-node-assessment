export default interface Image {
  id: string | number;
  path: string;
  main?: boolean;
};
