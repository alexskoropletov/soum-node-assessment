export default interface StateToState {
  fromStateId: string;
  toStateId: string;
};
