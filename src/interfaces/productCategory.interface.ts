export default interface ProductCategory {
  productId: string;
  categoryId: string;
};
