import { Service } from 'typedi';
//
import { Category, Product } from '../interfaces';
import { CategoryService } from './category.service';
import { ProductService } from './product.service';

@Service()
export class GraphQlService {
  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
  ) {}

  public async category(parent?: string | number): Promise<Category[] | undefined> {
    return this.categoryService.getList(parent);
  }

  public async product(state?: string | number): Promise<Product[] | undefined> {
    return this.productService.getList(state);
  }

  public async setProductState(productId: string | number, toState: string | number): Promise<Product> {
    return this.productService.setState(productId, toState);
  }
}