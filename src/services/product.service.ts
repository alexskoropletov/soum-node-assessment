import { Service } from 'typedi';
//
import { Product, Image } from '../interfaces';
import { DbService } from './db.service';
import { ImageService } from './image.service';

@Service()
export class ProductService {
  constructor(private db: DbService, private imageService: ImageService) {}

  public async get(id: string | number): Promise<Product> {
    const sql = `
      SELECT 
        p.id,
        p.name,
        p.price,
        c.name AS category,
        s.name AS state
      FROM product p
      LEFT JOIN product_category pc ON pc.product_id = p.id
      LEFT JOIN category c ON pc.category_id = c.id
      LEFT JOIN product_state ps ON ps.product_id = p.id
      LEFT JOIN state s ON ps.state_id = s.id
      WHERE p.id = $1
    `;
    const { rows } = await this.db.query(sql, [id]);

    const result = rows[0];
    if (result) {
      result.images = await this.imageService.getByProduct(id);
    }
    
    return result;
  }

  public async getList(state?: string | number): Promise<Product[] | undefined> {
    const sql = [`
      SELECT 
        p.id,
        p.name,
        p.price,
        c.name AS category,
        s.name AS state
      FROM product p
      LEFT JOIN product_category pc ON pc.product_id = p.id
      LEFT JOIN category c ON pc.category_id = c.id
      LEFT JOIN product_state ps ON ps.product_id = p.id
      LEFT JOIN state s ON ps.state_id = s.id
    `];
    let rows: Product[] = [];
    if (typeof state !== 'undefined') {
      sql.push('WHERE s.id = $1');
      ({ rows } = await this.db.query(sql.join(' '), [state]));
    } else {
      ({ rows } = await this.db.query(sql.join(' ')));
    }

    const result = rows.length ? rows : undefined;
    if (rows.length) {
      for(const item of result!) {
        item.images = await this.imageService.getByProduct(item.id);
      }
    }
    
    return result;
  }

  public async setState(productId: string | number, state: string | number): Promise<Product> {
    let sql = 'SELECT * FROM public.product_set_state($1, $2);';
    try {
    const { rows } = await this.db.query(sql, [productId, state]);
    
    return this.get((rows[0] ?? {}).product_id);
    } catch (e) {
      console.error(e);
      throw `Can't change product ${productId} state to ${state}`;
    }
  }
}