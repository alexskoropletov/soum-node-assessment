import { Service } from 'typedi';
//
import { DbService } from './db.service';
import { Category } from '../interfaces';

@Service()
export class CategoryService {
  constructor(private db: DbService) {}

  public async getList(parent?: string | number): Promise<Category[] | undefined> {
    let sql = [`
      SELECT 
        c.id,
        c.name,
        c.parent,
        p.name AS "parentName"
      FROM category c
      LEFT JOIN category p ON c.parent = p.id
    `];
    let rows: Category[] = [];
    if (typeof parent !== 'undefined' && parent) {
      sql.push('WHERE c.parent = $1');
      ({ rows } = await this.db.query(sql.join(' '), [parent]));
    } else {
      ({ rows } = await this.db.query(sql.join(' ')));
    }
    
    return rows.length ? rows : undefined;
  }
}