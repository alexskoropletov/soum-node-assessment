import { Client, ClientConfig } from 'pg';
import { Service } from 'typedi';
import config from '../config';

@Service({ global: true, multiple: false })
export class DbService {
  private client: Client;

  constructor() {
    this.client = new Client(config.db as ClientConfig);
  }

  public async connect() {
    return this.client.connect();
  }

  public async query(sql: string, args?: (string | number)[]) {
    return this.client.query(sql, args);
  }
}