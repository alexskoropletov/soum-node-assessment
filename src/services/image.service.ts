import { Service } from 'typedi';
//
import { DbService } from './db.service';
import { Image } from '../interfaces';

@Service()
export class ImageService {
  constructor(private db: DbService) {}

  public async getByProduct(productId?: number | string): Promise<Image[] | undefined> {
    if (!productId) {
      return undefined;
    }

    let sql = `
      SELECT 
        i.id,
        i.path,
        pi.main
      FROM image i
      INNER JOIN product_image pi ON pi.image_id = i.id
      WHERE
        pi.product_id = $1
    `;
    const { rows } = await this.db.query(sql, [productId]);
    
    return rows.length ? rows : undefined;
  }
}