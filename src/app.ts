import 'reflect-metadata';
import bodyParser from 'body-parser';
import express from 'express';
import Container from 'typedi';
import { useExpressServer, useContainer as routingUseContainer } from 'routing-controllers';
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
// 
import config from './config';
import graphqlHTTP from './graphql';
import { DbService } from './services/db.service';
import { ProductController } from './controllers/product.controller';
import { CategoryController } from './controllers/category.controller';

const swaggerDocument = swaggerJsdoc({
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Soum Swagger',
      version: '1.0.0',
    },
  },
  apis: ['./src/controllers/*.ts'],
});

const app = express();
try {
  (async () => {
    routingUseContainer(Container);
    const dbService = Container.get(DbService);
    await dbService.connect();
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.use('/graphql', graphqlHTTP)
    useExpressServer(app, {
      defaultErrorHandler: false,
      classTransformer: true,
      validation: true,
      routePrefix: config.apiPrefix,
      controllers: [
        ProductController,
        CategoryController,
      ],
    });
    app.listen(config.listenPort);
  })();  
} catch (e) {
  console.error(e);
  process.exit(1);
}

export default app;
