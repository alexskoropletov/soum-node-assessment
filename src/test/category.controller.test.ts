import 'reflect-metadata';
import sinon from 'sinon';
import Container from "typedi";
// 
import { CategoryController } from "../controllers/category.controller";
import { ProductController } from '../controllers/product.controller';
import { CategoryService } from "../services/category.service";
import { ProductService } from "../services/product.service";
import { DbService } from "../services/db.service";

const dbService = Container.get(DbService);

test('DB', async () => {
  const fake = sinon.fake();
  sinon.replace(dbService, "connect", fake);
  await dbService.connect();
  expect(fake.called).toBe(true);
});

test('CategoryController should call a service method', async () => {
  const fake = sinon.fake.resolves({ rows: [] });
  sinon.replace(dbService, "query", fake);
  const spy = sinon.spy(Container.get(CategoryService), 'getList');

  await Container.get(CategoryController).getAll();

  expect(fake.called).toBe(true);
  expect(spy.callCount).toBe(1);
});

test('CategoryController should call a service method a given parameter', async () => {
  const fake = sinon.fake.resolves({ rows: [] });
  sinon.replace(dbService, "query", fake);
  const spy = sinon.spy(Container.get(CategoryService), 'getList');

  await Container.get(CategoryController).getByParent(123);
  
  expect(fake.called).toBe(true);
  expect(spy.callCount).toBe(1);
  expect(spy.calledWith(123)).toBe(true);
});

test('ProductController should call a service method', async () => {
  const fake = sinon.fake.resolves({ rows: [] });
  sinon.replace(dbService, "query", fake);
  const spy = sinon.spy(Container.get(ProductService), 'getList');

  await Container.get(ProductController).getAll();

  expect(fake.called).toBe(true);
  expect(spy.callCount).toBe(1);
});

test('ProductController should call a service method with a given parameter', async () => {
  const fake = sinon.fake.resolves({ rows: [] });
  sinon.replace(dbService, "query", fake);
  const spy = sinon.spy(Container.get(ProductService), 'getList');

  await Container.get(ProductController).getByState(321);
  
  expect(fake.called).toBe(true);
  expect(spy.callCount).toBe(1);
  expect(spy.calledWith(321)).toBe(true);
});

afterEach(() => {
  sinon.restore();
});