import dotenv from 'dotenv';

dotenv.config();

export default Object.freeze({
  apiPrefix: 'api',
  listenPort: 3000,
  db: Object.freeze({
    user: process.env.DB_USERNAME || 'postgres',
    database: process.env.DB_NAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    port: process.env.DB_PORT || 5432,
    host: process.env.DB_HOST || 'localhost'
  })
});