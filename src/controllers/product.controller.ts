import { JsonController, Get, Param, OnUndefined, Put } from "routing-controllers";
import { ResponseSchema } from "routing-controllers-openapi";
import { Inject, Service } from "typedi";
//
import { ProductService } from "../services/product.service";

@Service()
@JsonController('/product')
export class ProductController {
  @Inject()
  private productService: ProductService;

  /**
  * @swagger
  *
  * /product:
  *   get:
  *     description: Returns a list of products
  *     produces:
  *       - application/json
  */
  @Get('/')
  @ResponseSchema('Error', {
    statusCode: 500,
    description: 'Unexpected error',
  })
  @OnUndefined(204)
  public getAll() {
    return this.productService.getList();
  }

  /**
  * @swagger
  *
  * /product/{state}:
  *   get:
  *     description: Returns a list of products in a given state
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: state
  *         in: path
  *         required: true
  *         type: string
  *         description: State ID
  */
  @Get('/:state')
  @ResponseSchema('Error', {
    statusCode: 500,
    description: 'Unexpected error',
  })
  @OnUndefined(204)
  public getByState(@Param('state') state: number | string) {
    return this.productService.getList(state);
  }

  /**
  * @swagger
  *
  * /product/{product_id}/{state}:
  *   put:
  *     description: Attemts to change state of a given product
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: product_id
  *         in: path
  *         required: true
  *         type: string
  *         description: Product ID
  *       - name: state
  *         in: path
  *         required: true
  *         type: string
  *         description: Next state ID
  */
  @Put('/:product_id/:state')
  @ResponseSchema('Error', {
    statusCode: 500,
    description: 'Unexpected error',
  })
  @OnUndefined(204)
  public setState(@Param('product_id') productId: string, @Param('state') state: string) {
    return this.productService.setState(productId, state);
  }
}