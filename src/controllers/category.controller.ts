import { JsonController, Get, Param, OnUndefined } from "routing-controllers";
import { ResponseSchema } from "routing-controllers-openapi";
import { Inject, Service } from "typedi";
//
import { CategoryService } from "../services/category.service";


@Service()
@JsonController('/category')
export class CategoryController {
  @Inject()
  private categoryService: CategoryService;

  /**
  * @swagger
  *
  * /category:
  *   get:
  *     description: Returns a list of cetegories
  *     produces:
  *       - application/json
  */
  @Get('/')
  @ResponseSchema('Error', {
    statusCode: 500,
    description: 'Unexpected error',
  })
  @OnUndefined(204)
  public getAll() {
    return this.categoryService.getList();
  }

  /**
  * @swagger
  *
  * /category/{parent}:
  *   get:
  *     description: Returns a list of cetegories belonging to a given perent category
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: parent
  *         in: path
  *         required: true
  *         type: string
  *         description: Parent category ID
  */
  @Get('/:parent')
  @ResponseSchema('Error', {
    statusCode: 500,
    description: 'Unexpected error',
  })
  @OnUndefined(204)
  public getByParent(@Param('parent') parent: string | number) {
    return this.categoryService.getList(parent);
  }
}