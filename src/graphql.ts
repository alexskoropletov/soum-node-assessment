import { graphqlHTTP } from 'express-graphql';
import { buildSchema } from 'graphql';
import Container from 'typedi';
import { GraphQlService } from './services/graphql.service.';

const schema = buildSchema(`  
  type Query {
    category(parent: ID): [Category],
    product(state: ID): [Product]
  }

  type Mutation {
    setProductState(product: ID, state: ID): Product
  }

  type Category {
    id: ID!
    name: String!
    parent: ID
  }

  type Product {
    id: ID!
    name: String!
    price: Float!
    state: String
    category: String
    images: [Image]
  }

  type Image {
    id: ID!
    path: String!
    main: Boolean
  }
`);

const graphQlService = Container.get(GraphQlService);

const rootValue = {
  category: async ({ parent }) => graphQlService.category(parent),
  product: async ({ state }) => graphQlService.product(state),
  setProductState: ({ product, state }) => graphQlService.setProductState(product, state),
};

export default graphqlHTTP({
  schema,
  rootValue,
  graphiql: true,
});