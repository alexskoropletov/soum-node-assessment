## Project Start

 - Set up local PostgreSQL Server
 - Put your local PostgreSQL Server credentials into .env file
 - run `npm install`
 - run `npm run start`

## Project Info

  - swagger: http://localhost:3000/swagger
  - graphiql: http://localhost:3000/graphql
  - api: http://localhost:3000/api
    - GET /category
    - GET /category/:parentId
    - GET /product
    - GET /product/:stateId
    - PUT /product/:productId/:stateId
  - you can run tests with `npm run test`