WITH parent AS (
	INSERT INTO public.category ("name") VALUES ('Electronics')
	RETURNING id
)
INSERT INTO public.category ("name", "parent") SELECT UNNEST(ARRAY['Phones', 'TVs', 'Smart Watches']), id FROM parent;

WITH parent AS (
	INSERT INTO public.category ("name") VALUES ('Vehicles')
	RETURNING id
)
INSERT INTO public.category ("name", "parent") SELECT UNNEST(ARRAY['Cars', 'Motorcycles']), id FROM parent;

WITH parent AS (
	INSERT INTO public.category ("name") VALUES ('Real Estate')
	RETURNING id
)
INSERT INTO public.category ("name", "parent") SELECT UNNEST(ARRAY['Appartments', 'Houses', 'Land']), id FROM parent;