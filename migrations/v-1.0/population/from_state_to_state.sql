INSERT INTO public.from_state_to_state ("from_state", "to_state") VALUES
(1, 2), -- Draft to Deleted Draft
(1, 3), -- Draft to Available
(3, 5), -- Availabe to Deleted
(3, 6), -- Available to Expired
(3, 7), -- Available to Reserved
(4, 8), -- Sold to Returned
(6, 3), -- Expired to Available
(7, 3), -- Reserved to Availabe
(7, 4), -- Reserved to Sold
(8, 1), -- Returned to Draft
(8, 5); -- Returned to Deleted