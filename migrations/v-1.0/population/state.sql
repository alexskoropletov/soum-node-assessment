INSERT INTO public.state ("name") SELECT UNNEST(ARRAY[
  'Draft',
  'Deleted Draft',
  'Available',
  'Sold',
  'Deleted',
  'Expired',
  'Reserved',
  'Returned'
]);