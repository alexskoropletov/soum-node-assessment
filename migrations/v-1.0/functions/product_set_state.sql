CREATE OR REPLACE FUNCTION public.product_set_state(
  p_product_id INTEGER,
  p_to_state INTEGER
) RETURNS TABLE (
  "product_id" INTEGER,
  "state_id" INTEGER
) AS
$$
BEGIN
  IF NOT EXISTS (
    SELECT 1
    FROM product
    WHERE id = p_product_id
  ) THEN
    RAISE EXCEPTION 'ProductNotFound';
  END IF;

  IF NOT EXISTS (
    SELECT 1
    FROM state
    WHERE id = p_to_state
  ) THEN
    RAISE EXCEPTION 'StateNotFound';
  END IF;

  IF EXISTS (
    SELECT 1
    FROM public.product_state AS ps
    WHERE
      ps.product_id = p_product_id
  ) THEN
    IF NOT EXISTS (
      SELECT 1
      FROM public.from_state_to_state fsts
      INNER JOIN public.product_state ps ON fsts.from_state = ps.state_id
      WHERE
        ps.product_id = p_product_id
        AND fsts.to_state = p_to_state
    ) THEN
      RAISE EXCEPTION 'InvalidStateChange';
    END IF;

    UPDATE public.product_state SET state_id = p_to_state WHERE public.product_state.product_id = p_product_id;
  ELSE
    INSERT INTO public.product_state ("product_id", "state_id") VALUES (p_product_id, p_to_state);
  END IF;

  INSERT INTO public.product_state_history ("product_id", "state_id", "update_at") VALUES (p_product_id, p_to_state, NOW()::TIMESTAMP);

  RETURN QUERY
    SELECT
      ps.product_id,
      ps.state_id
    FROM public.product_state AS ps
    WHERE
      ps.product_id = p_product_id;
END
$$
LANGUAGE 'plpgsql';