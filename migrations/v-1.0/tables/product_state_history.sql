﻿CREATE TABLE public.product_state_history(
	"product_id" INTEGER NOT NULL,
	"state_id" INTEGER NOT NULL,
  "update_at" TIMESTAMP,
	CONSTRAINT fk_product_state_history_product FOREIGN KEY("product_id") REFERENCES public.product("id"),
	CONSTRAINT fk_product_state_history_state FOREIGN KEY("state_id") REFERENCES public.state("id")
);
