﻿CREATE TABLE public.product_category(
	"product_id" INTEGER NOT NULL,
	"category_id" INTEGER NOT NULL,
	CONSTRAINT fk_product_category_product FOREIGN KEY("product_id") REFERENCES public.product("id"),
	CONSTRAINT fk_product_category_category FOREIGN KEY("category_id") REFERENCES public.category("id")
);
