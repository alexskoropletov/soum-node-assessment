﻿CREATE TABLE public.from_state_to_state(
	"from_state" INTEGER NOT NULL,
	"to_state" INTEGER NOT NULL,
	CONSTRAINT fk_from_state_to_state_from_state FOREIGN KEY("from_state") REFERENCES public.state("id"),
	CONSTRAINT fk_from_state_to_state_to_state FOREIGN KEY("to_state") REFERENCES public.state("id")
);
