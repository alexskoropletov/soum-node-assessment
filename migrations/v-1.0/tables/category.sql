﻿CREATE TABLE public.category(
	"id" SERIAL,
	"name" TEXT NOT NULL,
	"parent" INTEGER,
	CONSTRAINT pk_category PRIMARY KEY("id")
);
