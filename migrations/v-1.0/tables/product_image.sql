﻿CREATE TABLE public.product_image(
	"product_id" INTEGER NOT NULL,
	"image_id" INTEGER NOT NULL,
	"main" BOOLEAN NOT NULL DEFAULT FALSE,
	CONSTRAINT fk_product_image_product FOREIGN KEY("product_id") REFERENCES public.product("id"),
	CONSTRAINT fk_product_image_image FOREIGN KEY("image_id") REFERENCES public.image("id")
);
