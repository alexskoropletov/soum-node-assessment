﻿CREATE TABLE public.product(
	"id" SERIAL,
	"name" TEXT NOT NULL,
	"price" FLOAT NOT NULL,
	CONSTRAINT pk_product PRIMARY KEY("id")
);
