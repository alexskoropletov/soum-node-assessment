﻿CREATE TABLE public.image(
	"id" SERIAL,
	"path" TEXT NOT NULL,
	CONSTRAINT pk_image PRIMARY KEY("id")
);
