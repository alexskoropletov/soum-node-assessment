﻿CREATE TABLE public.state(
	"id" SERIAL,
	"name" TEXT NOT NULL,
	CONSTRAINT pk_state PRIMARY KEY("id")
);
