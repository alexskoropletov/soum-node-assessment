﻿CREATE TABLE public.product_state(
	"product_id" INTEGER NOT NULL,
	"state_id" INTEGER NOT NULL,
	CONSTRAINT fk_product_state_product FOREIGN KEY("product_id") REFERENCES public.product("id"),
	CONSTRAINT fk_product_state_state FOREIGN KEY("state_id") REFERENCES public.state("id")
);
