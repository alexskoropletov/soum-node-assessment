const dotenv = require('dotenv');
const { Liquibase } = require('node-liquibase');
const { Client } = require('pg');

dotenv.config();

async function createDatabaseIfNotExists(ctx, dbname) {
  const client = new Client({
    host: ctx.host,
    port: ctx.port,
    user: ctx.username,
    password: ctx.password,
    database: 'postgres',
    ssl: ctx.ssl
  });

  await client.connect();
  const res = await client.query("SELECT datname FROM pg_database WHERE datname = $1", [dbname]);
  if (res.rows.length === 0) {
    console.log('Creating database...');
    await client.query(`CREATE DATABASE ${client.escapeIdentifier(dbname)}`);
    console.log('Database ' + dbname + ' was created.');
  }
  await client.end();
}

async function applyChangelog(ctx, dbname, changeLogFile) {
  const lb = new Liquibase({
    changeLogFile,
    url: 'jdbc:postgresql://' + ctx.host + ':' + ctx.port + '/' + dbname + (ctx.ssl ? '?ssl=true' : ''),
    username: ctx.username,
    password: fixLiquibasePassword(ctx.password)
  });
  lb.run('update');
  console.log('Changes were successfully applied on database ' + dbname);
}

function fixLiquibasePassword(password) {
  let replaceAll = function (target, search, replacement) {
    return target.split(search).join(replacement);
  };

  let fix = function (pwd, symbols, index) {
    if (index == symbols.length) {
      return pwd;
    } else {
      return fix(replaceAll(pwd, symbols[index], '\\' + symbols[index]), symbols, index + 1);
    }
  };

  return fix(password, '@<([{^-=$!|]})?*+.>~;&', 0);
}

(async () => {
  let context = {
    host: process.env.DB_HOST ?? 'localhost',
    port: process.env.DB_PORT ?? '5433',
    appDbName: process.env.DB_NAME ?? 'postgres',
    username: process.env.DB_USERNAME ?? 'postgres',
    password: process.env.DB_PASSWORD ?? 'postgres',
    ssl: false
  };

  try {
    await createDatabaseIfNotExists(context, context.appDbName);
    await applyChangelog(context, context.appDbName, 'migrations/changelog.xml');
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();

